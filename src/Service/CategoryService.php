<?php

namespace App\Service;

use App\Dto\CategoryDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Category;
use App\Entity\User;
use App\Filter\CategoryFilter;
use App\Repository\CategoryRepository;

class CategoryService
{
    public function __construct(private readonly CategoryRepository $categoryRepository, private readonly DtoConverter $dtoConverter) {}

    /**
     * @return CategoryDto[]
     *
     * @throws \Exception
     */
    public function getCategoriesByUser(User $user, CategoryFilter $categoryFilter = null): array
    {
        $categories = $this->categoryRepository->getCategoriesByUser($user, $categoryFilter);

        return array_map(function (Category $category) {
            return $this->dtoConverter->convertToDto($category);
        }, $categories);
    }

    /**
     * @return CategoryDto
     *
     * @throws \Exception
     */
    public function save(CategoryDto $categoryDto, User $user): object
    {
        /** @var Category $category */
        $category = $this->dtoConverter->convertToEntity($categoryDto);
        $category->setUser($user);

        $category = $this->categoryRepository->save($category);

        return $this->dtoConverter->convertToDto($category);
    }

    /**
     * @return CategoryDto
     *
     * @throws \Exception
     */
    public function update(CategoryDto $categoryDto, Category $category): object
    {
        $category->setName($categoryDto->name);
        $category = $this->categoryRepository->update($category);

        return $this->dtoConverter->convertToDto($category);
    }

    public function delete(Category $category): bool
    {
        $this->categoryRepository->delete($category);

        return true;
    }
}
