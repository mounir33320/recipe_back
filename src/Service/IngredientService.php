<?php

namespace App\Service;

use App\Dto\IngredientDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Ingredient;
use App\Entity\User;
use App\Filter\IngredientFilter;
use App\Repository\IngredientRepository;

class IngredientService
{
    public function __construct(private readonly IngredientRepository $ingredientRepository, private readonly DtoConverter $dtoConverter) {}

    public function getIngredientsByUser(User $user, IngredientFilter $ingredientFilter = null): array
    {
        $categories = $this->ingredientRepository->getIngredientsByUser($user, $ingredientFilter);

        return array_map(function (Ingredient $ingredient) {
            return $this->dtoConverter->convertToDto($ingredient);
        }, $categories);
    }

    public function save(IngredientDto $ingredientDto, User $user): object
    {
        /** @var Ingredient $ingredient */
        $ingredient = $this->dtoConverter->convertToEntity($ingredientDto);
        $ingredient->setUser($user);
        $ingredient = $this->ingredientRepository->save($ingredient);

        return $this->dtoConverter->convertToDto($ingredient);
    }

    /**
     * @return IngredientDto
     *
     * @throws \Exception
     */
    public function update(IngredientDto $ingredientDto, Ingredient $ingredient): object
    {
        $ingredient->setName($ingredientDto->name);
        $ingredient = $this->ingredientRepository->update($ingredient);

        return $this->dtoConverter->convertToDto($ingredient);
    }

    public function delete(Ingredient $ingredient): bool
    {
        $this->ingredientRepository->delete($ingredient);

        return true;
    }
}
