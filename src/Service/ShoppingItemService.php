<?php

namespace App\Service;

use App\Dto\ShoppingItemDto;
use App\DtoConverter\DtoConverter;
use App\Entity\ShoppingItem;
use App\Entity\User;
use App\Repository\ShoppingItemRepository;

class ShoppingItemService
{
    public function __construct(private readonly DtoConverter $dtoConverter, private readonly ShoppingItemRepository $shoppingItemRepository)
    {
    }

    public function getShoppingItemsByUser(User $user): array
    {
        return $this->dtoConverter->convertArrayOfEntitiesToDtos($user->getShoppingItems());
    }

    /**
     * @param ShoppingItemDto $shoppingItemDto
     * @param User $user
     * @return ShoppingItemDto
     * @throws \Exception
     */
    public function save(ShoppingItemDto $shoppingItemDto, User $user): object
    {
        /** @var ShoppingItem $shoppingItem */
        $shoppingItem = $this->dtoConverter->convertToEntity($shoppingItemDto);
        $shoppingItem->setUser($user);

        $shoppingItem = $this->shoppingItemRepository->save($shoppingItem);

        return $this->dtoConverter->convertToDto($shoppingItem);
    }

    public function update(ShoppingItemDto $shoppingItemDto, ShoppingItem $shoppingItem): object
    {
        $shoppingItem->setName($shoppingItemDto->name)->setActive($shoppingItemDto->isActive);
        $shoppingItem = $this->shoppingItemRepository->update($shoppingItem);

        return $this->dtoConverter->convertToDto($shoppingItem);
    }

    public function delete(ShoppingItem $shoppingItem): bool
    {
        $this->shoppingItemRepository->delete($shoppingItem);
        return true;
    }
}
