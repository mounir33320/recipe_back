<?php

namespace App\Service;

use App\Dto\RecipeDto;
use App\DtoConverter\DtoConverter;
use App\DtoConverter\DtoConverterInterface;
use App\Entity\Category;
use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Entity\User;
use App\Filter\RecipeFilter;
use App\Repository\CategoryRepository;
use App\Repository\IngredientRepository;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RecipeService
{
    public function __construct(
        private readonly DtoConverter $dtoConverter,
        private readonly RecipeRepository $recipeRepository,
        private readonly IngredientRepository $ingredientRepository,
        private readonly CategoryRepository $categoryRepository,
        private readonly UserRepository $userRepository)
    {
    }

    public function getRecipesByUser(User $user, RecipeFilter $recipeFilter = null): array
    {
        $recipes = $this->recipeRepository->getRecipesByUser($user, $recipeFilter);

        return array_map(function(Recipe $recipe) {
            return $this->dtoConverter->convertToDto($recipe);
        }, $recipes);
    }

    /**
     * @param RecipeDto $recipeDto
     * @param User $user
     * @return RecipeDto
     * @throws \Exception
     */
    public function save(RecipeDto $recipeDto, User $user): object
    {
        $recipe = $this->dtoConverter->convertToEntity($recipeDto);
        $recipe->setUser($user);


        $ingredients = $this->buildArrayOfEntities($recipeDto->ingredients, $this->ingredientRepository);
        $categories = $this->buildArrayOfEntities($recipeDto->categories, $this->categoryRepository);

        $recipe = $this->addIngredientsToRecipe($recipe, $ingredients);
        $recipe = $this->addCategoriesToRecipe($recipe, $categories);
        $recipe = $this->recipeRepository->save($recipe);

        return $this->dtoConverter->convertToDto($recipe);
    }

    public function update(RecipeDto $recipeDto, Recipe $recipe): RecipeDto
    {
        $recipe
            ->setTitle($recipeDto->title)
            ->setComment($recipeDto->comment)
        ;

        $ingredients = $this->buildArrayOfEntities($recipeDto->ingredients, $this->ingredientRepository);
        $categories = $this->buildArrayOfEntities($recipeDto->categories, $this->categoryRepository);
        $recipe = $this->addIngredientsToRecipe($recipe, $ingredients);
        $recipe = $this->addCategoriesToRecipe($recipe, $categories);
        $recipe = $this->recipeRepository->update($recipe);

        return $this->dtoConverter->convertToDto($recipe);
    }

    public function delete(Recipe $recipe): bool
    {
        $this->recipeRepository->delete($recipe);
        return true;
    }


    private function buildArrayOfEntities(array $dtos, ServiceEntityRepositoryInterface $repository): array
    {
        return array_map(function($dto) use ($repository) {
            return $dto->id ?  $repository->find($dto->id) : $this->dtoConverter->convertToEntity($dto);

        }, $dtos);
    }

    /**
     * @param Recipe $recipe
     * @param Ingredient[] $ingredients
     * @return Recipe
     */
    private function addIngredientsToRecipe(Recipe $recipe, array $ingredients): Recipe
    {
        foreach ($ingredients as $ingredient) {
            $ingredient->setUser($recipe->getUser());
            $recipe->addIngredient($ingredient);
        }

        return $recipe;
    }

    /**
     * @param Recipe $recipe
     * @param Category[] $categories
     * @return Recipe
     */
    private function addCategoriesToRecipe(Recipe $recipe, array $categories): Recipe
    {
        foreach ($categories as $category) {
            $category->setUser($recipe->getUser());
            $recipe->addCategory($category);
        }

        return $recipe;
    }
}
