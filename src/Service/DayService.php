<?php

namespace App\Service;

use App\Dto\DayDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Day;
use App\Entity\RecipeDay;
use App\Entity\User;
use App\Enum\DayEnum;
use App\Enum\MealTypeEnum;
use App\Repository\DayRepository;
use App\Repository\RecipeDayRepository;
use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DayService
{
    public function __construct(
        private readonly DayRepository $dayRepository,
        private readonly DtoConverter $dtoConverter,
        private readonly RecipeRepository $recipeRepository,
        private readonly RecipeDayRepository $recipeDayRepository)
    {
    }

    public function initDays(User $user): array
    {
        $days = [];
        foreach (DayEnum::cases() as $dayEnum) {
            if ($this->isDayExists($dayEnum, $user)) {
                throw new \Exception('Days already initialized');
            }

            $dayNumber = (int) date('N', strtotime($dayEnum->name));
            $day = (new Day())
                ->setDayNumber($dayNumber)
                ->setName($dayEnum)
                ->setUser($user);
            $days[] = $this->dayRepository->save($day, false);
        }
        $this->dayRepository->flush();

        return $this->convertArrayOfDaysToDto($days);
    }

    public function getDaysByUser(User $user): array
    {
        return $this->convertArrayOfDaysToDto($user->getDays()->toArray());
    }

    /**
     * @param DayDto $dayDto
     * @param Day $day
     * @return DayDto
     * @throws \Exception
     */
    public function update(DayDto $dayDto, Day $day): object
    {
        $recipesDaysToRemove = clone $day->getRecipesDays();

        foreach ($dayDto->recipes as $recipeDayArray) {
            $recipeDayAlreadyExists = $day->getRecipesDays()->filter(function(RecipeDay $recipeDay) use ($recipeDayArray) {
                 return $recipeDay->getRecipe()->getId() === $recipeDayArray['id'] && $recipeDay->getType() === MealTypeEnum::from($recipeDayArray['type']);
            });

            if (!$recipeDayAlreadyExists->isEmpty()) {
                $recipesDaysToRemove->removeElement($recipeDayAlreadyExists->first());
                continue;
            }

            $type = MealTypeEnum::from($recipeDayArray['type']);
            $recipe = $this->recipeRepository->find($recipeDayArray['id']);
            $recipeDay = (new RecipeDay())
                ->setDay($day)
                ->setType($type)
                ->setRecipe($recipe)
            ;
            $day->addRecipesDay($recipeDay);
        }

        foreach ($recipesDaysToRemove as $recipeDayToRemove) {
            $day->removeRecipesDay($recipeDayToRemove);
        }

        $day = $this->dayRepository->update($day);

        return $this->dtoConverter->convertToDto($day);
    }

    private function isDayExists(DayEnum $dayEnum, User $user): bool
    {
        return $user->getDays()->exists(function(int $index, Day $day) use ($dayEnum) {
           return $day->getName() === $dayEnum;
        });
    }

    /**
     * @param Day[] $days
     * @return DayDto[]
     * @throws \Exception
     */
    private function convertArrayOfDaysToDto(array $days): array
    {
        $daysDto = [];
        foreach ($days as $day) {
            $daysDto[] = $this->dtoConverter->convertToDto($day);
        }
        return $daysDto;
    }
}
