<?php

namespace App\DataFixtures;

use App\Entity\Recipe;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
;

class RecipeFixtures extends Fixture
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $user = $this->userRepository->find(3);
        for ($i = 0; $i < 20; $i++) {
            $recipe = (new Recipe())
                ->setTitle("Titre $i")
                ->setComment("Commentaire $i")
                ->setUser($user);
            $manager->persist($recipe);
        }

        $manager->flush();
    }
}
