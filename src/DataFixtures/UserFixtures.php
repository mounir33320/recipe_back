<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

;

class UserFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $hasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
//        $user = (new User())
//            ->setUsername("mounir")
//            ->setPassword($this->hasher->hashPassword(new User(), 'blabla'))
//            ->setRoles(['ROLE_USER'])
//            ->setActive(true)
//        ;
//
//        $manager->persist($user);
//        $manager->flush();
    }
}
