<?php

namespace App\Repository;

use App\Entity\Day;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Day>
 *
 * @method Day|null find($id, $lockMode = null, $lockVersion = null)
 * @method Day|null findOneBy(array $criteria, array $orderBy = null)
 * @method Day[]    findAll()
 * @method Day[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Day::class);
    }

    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    public function save(Day $day, bool $flush = true): Day
    {
        $this->getEntityManager()->persist($day);
        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $day;
    }

    public function update(Day $day): Day
    {
        $this->getEntityManager()->flush();
        return $day;
    }
}
