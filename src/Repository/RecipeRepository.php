<?php

namespace App\Repository;

use App\Entity\Recipe;
use App\Entity\User;
use App\Filter\RecipeFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Recipe>
 *
 * @method Recipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recipe[]    findAll()
 * @method Recipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recipe::class);
    }

    public function getRecipesByUser(User $user, ?RecipeFilter $recipeFilter = null): array
    {
        return $this->findBy(['user' => $user]);
    }

    public function save(Recipe $recipe): Recipe
    {
        $this->getEntityManager()->persist($recipe);
        $this->getEntityManager()->flush();

        return $recipe;
    }

    public function update(Recipe $recipe): Recipe
    {
        $this->getEntityManager()->flush();

        return $recipe;
    }

    public function delete(Recipe $recipe): bool
    {
        $this->getEntityManager()->remove($recipe);
        $this->getEntityManager()->flush();
        return true;
    }
}
