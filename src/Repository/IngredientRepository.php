<?php

namespace App\Repository;

use App\Entity\Ingredient;
use App\Entity\User;
use App\Filter\IngredientFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ingredient>
 *
 * @method Ingredient|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ingredient|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ingredient[]    findAll()
 * @method Ingredient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IngredientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ingredient::class);
    }

    public function getIngredientsByUser(User $user, ?IngredientFilter $ingredientFilter = null): array
    {
        return $this->findBy(['user' => $user]);
    }

    public function save(Ingredient $ingredient): Ingredient
    {
        $this->getEntityManager()->persist($ingredient);
        $this->getEntityManager()->flush();

        return $ingredient;
    }

    public function update(Ingredient $ingredient): Ingredient
    {
        $this->getEntityManager()->flush();
        return $ingredient;
    }

    public function delete(Ingredient $ingredient): bool
    {
        $this->getEntityManager()->remove($ingredient);
        $this->getEntityManager()->flush();

        return true;
    }
}
