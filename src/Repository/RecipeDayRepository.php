<?php

namespace App\Repository;

use App\Entity\RecipeDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RecipeDay>
 *
 * @method RecipeDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeDay[]    findAll()
 * @method RecipeDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeDayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeDay::class);
    }

    public function findByIds(array $ids)
    {
        return $this->createQueryBuilder('recipe_day')
            ->where('recipe_day.id IN :ids')
            ->setParameter('ids', $ids)
            ->getQuery()->getResult()
            ;
    }

    public function save(RecipeDay $recipeDay, bool $flush = true): RecipeDay
    {
        $this->getEntityManager()->persist($recipeDay);
        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $recipeDay;
    }
}
