<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\User;
use App\Filter\CategoryFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }


    public function getCategoriesByUser(User $user, CategoryFilter $categoryFilter = null): array
    {
        return $this->findBy(['user' => $user]);
    }

    public function save(Category $category): Category
    {
        $this->getEntityManager()->persist($category);
        $this->getEntityManager()->flush();

        return $category;
    }

    public function update(Category $category): Category
    {
        $this->getEntityManager()->flush();
        return $category;
    }

    public function delete(Category $category): bool
    {
        $this->getEntityManager()->remove($category);
        $this->getEntityManager()->flush();

        return true;
    }
}
