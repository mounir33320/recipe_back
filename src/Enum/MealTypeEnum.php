<?php

namespace App\Enum;

enum MealTypeEnum: string
{
    case lunch = 'lunch';
    case dinner = 'dinner';
}
