<?php

namespace App\Dto;

class DayDto
{
    public function __construct(
        public ?int $id,
        public ?string $name,
        public ?int $dayNumber,
        public ?array $recipes = [])
    {
    }
}
