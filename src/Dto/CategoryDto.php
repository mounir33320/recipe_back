<?php

namespace App\Dto;

class CategoryDto
{
    public function __construct(public ?int $id, public string $name, public ?int $userId)
    {
    }
}
