<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints\NotBlank;

class RecipeDto
{
    public function __construct(
        public ?int $id,
        public string $title,
        public string $comment,
        public ?int $userId,
        /**
         * @var IngredientDto[]
         */
        public array $ingredients = [],
        /**
         * @var CategoryDto[]
         */
        public array $categories = []
        )
    {
    }
}
