<?php

namespace App\Dto;

class ShoppingItemDto
{
    public function __construct(
        public ?int $id,
        public ?string $name,
        public ?bool $isActive
    )
    {
    }
}
