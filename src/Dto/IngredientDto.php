<?php

namespace App\Dto;

class IngredientDto
{
    public function __construct(public ?int $id, public string $name, public ?int $userId, public array $recipes = [])
    {
    }
}
