<?php

namespace App\Dto;

class UserDto
{
    public function __construct(
        public ?int $id,
        public ?string $username,
        public ?string $password,
        public ?array $roles,
        public bool $isActive = true,
        public ?string $lastConnection = null)
    {
    }
}
