<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class AppAuthenticator extends AbstractAuthenticator
{
    public function __construct(private readonly RequestStack $requestStack, private readonly UserRepository $userRepository)
    {
    }

    public function supports(Request $request ): ?bool
    {
        $currentSessionId = $request->cookies->get('PHPSESSID');
        return !empty($currentSessionId);
    }

    public function authenticate(Request $request): Passport
    {
        $sessionId = $request->cookies->get('PHPSESSID');
        if ($this->requestStack->getSession()->getId() !== $sessionId) {
            throw new CustomUserMessageAuthenticationException('Session has expired');
        }

        /** @var UsernamePasswordToken $usernamePasswordToken */
        $usernamePasswordToken = unserialize($this->requestStack->getSession()->get('_security_main'));
        /** @var User $user */
        $user = $usernamePasswordToken->getUser();

        if (!$user->isActive()) {
            throw new CustomUserMessageAuthenticationException('Account deactivate');
        }

        $user->setLastConnection(new \DateTimeImmutable());
        $this->userRepository->update($user);

        return new SelfValidatingPassport(new UserBadge($user->getUserIdentifier()));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $error = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new JsonResponse(['success' => false, 'error' => $error], Response::HTTP_UNAUTHORIZED);
    }
}
