<?php

namespace App\Entity;

use App\Enum\DayEnum;
use App\Repository\DayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DayRepository::class)]
class Day
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 15)]
    private ?DayEnum $name = null;

    #[ORM\Column]
    private ?int $dayNumber = null;

    #[ORM\ManyToOne(inversedBy: 'days')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'day', targetEntity: RecipeDay::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $recipesDays;

    public function __construct()
    {
        $this->recipesDays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?DayEnum
    {
        return $this->name;
    }

    public function setName(DayEnum $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDayNumber(): ?int
    {
        return $this->dayNumber;
    }

    public function setDayNumber(int $dayNumber): static
    {
        $this->dayNumber = $dayNumber;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, RecipeDay>
     */
    public function getRecipesDays(): Collection
    {
        return $this->recipesDays;
    }

    public function addRecipesDay(RecipeDay $recipesDay): static
    {
        if (!$this->recipesDays->contains($recipesDay)) {
            $this->recipesDays->add($recipesDay);
            $recipesDay->setDay($this);
        }

        return $this;
    }

    public function removeRecipesDay(RecipeDay $recipesDay): static
    {
        if ($this->recipesDays->removeElement($recipesDay)) {
            // set the owning side to null (unless already changed)
            if ($recipesDay->getDay() === $this) {
                $recipesDay->setDay(null);
            }
        }

        return $this;
    }
}
