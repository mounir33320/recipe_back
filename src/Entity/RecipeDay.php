<?php

namespace App\Entity;

use App\Enum\MealTypeEnum;
use App\Repository\RecipeDayRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipeDayRepository::class)]
class RecipeDay
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 15)]
    private ?MealTypeEnum $type = null;

    #[ORM\ManyToOne(inversedBy: 'recipesDays')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Day $day = null;

    #[ORM\ManyToOne(inversedBy: 'recipesDays')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Recipe $recipe = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): MealTypeEnum
    {
        return $this->type;
    }

    public function setType(MealTypeEnum $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getDay(): ?Day
    {
        return $this->day;
    }

    public function setDay(?Day $day): static
    {
        $this->day = $day;

        return $this;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): static
    {
        $this->recipe = $recipe;

        return $this;
    }
}
