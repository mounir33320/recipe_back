<?php

namespace App\Entity;

use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecipeRepository::class)]
class Recipe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $comment = null;

    #[ORM\ManyToOne(inversedBy: 'recipes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToMany(targetEntity: Ingredient::class, inversedBy: 'recipes', cascade: ['persist'])]
    private Collection $ingredients;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'recipes', cascade: ['persist'])]
    private Collection $categories;

    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: RecipeDay::class, orphanRemoval: true)]
    private Collection $recipesDays;

    public function __construct()
    {
        $this->ingredients = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->recipesDays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Ingredient>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): static
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): static
    {
        $this->ingredients->removeElement($ingredient);

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): static
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategory(Category $category): static
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * @return Collection<int, RecipeDay>
     */
    public function getRecipesDays(): Collection
    {
        return $this->recipesDays;
    }

    public function addRecipesDay(RecipeDay $recipesDay): static
    {
        if (!$this->recipesDays->contains($recipesDay)) {
            $this->recipesDays->add($recipesDay);
            $recipesDay->setRecipe($this);
        }

        return $this;
    }

    public function removeRecipesDay(RecipeDay $recipesDay): static
    {
        if ($this->recipesDays->removeElement($recipesDay)) {
            // set the owning side to null (unless already changed)
            if ($recipesDay->getRecipe() === $this) {
                $recipesDay->setRecipe(null);
            }
        }

        return $this;
    }
}
