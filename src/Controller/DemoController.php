<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemoController extends AbstractController
{
    #[Route('/api/demo', name: 'demo')]
    public function demo()
    {
        return $this->json(['data' => 'blabla']);
    }
}
