<?php

namespace App\Controller;

use App\Dto\CategoryDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Category;
use App\Entity\User;
use App\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/categories', name: 'categories_')]
class CategoryController extends AbstractController
{
    public function __construct(private readonly CategoryService $categoryService, private readonly NormalizerInterface $normalizer) {}

    #[Route('', name: 'index', methods: 'GET')]
    public function index(#[CurrentUser] User $user): JsonResponse
    {
        $categories = $this->categoryService->getCategoriesByUser($user);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($categories)], Response::HTTP_OK);
    }

    #[Route('', name: 'create', methods: 'POST')]
    public function create(#[MapRequestPayload] CategoryDto $categoryDto, #[CurrentUser] User $user): JsonResponse
    {
        $category = $this->categoryService->save($categoryDto, $user);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($category)], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'show', methods: 'GET')]
    public function show(Category $category, DtoConverter $dtoConverter): JsonResponse
    {
        $categoryDto = $dtoConverter->convertToDto($category);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($categoryDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'update', methods: 'PUT')]
    public function update(#[MapRequestPayload] CategoryDto $categoryDto, Category $category): JsonResponse
    {
        $categoryDto = $this->categoryService->update($categoryDto, $category);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($categoryDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    public function delete(Category $category): JsonResponse
    {
        $this->categoryService->delete($category);

        return $this->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
