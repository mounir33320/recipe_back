<?php

namespace App\Controller;

use App\Dto\ShoppingItemDto;
use App\Entity\ShoppingItem;
use App\Entity\User;
use App\Service\ShoppingItemService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/shopping-items', name: 'shopping_items_')]
class ShoppingItemController extends AbstractController
{
    public function __construct(private readonly ShoppingItemService $shoppingItemService, private readonly NormalizerInterface $normalizer)
    {
    }

    #[Route('', name: 'index', methods: 'GET')]
    public function index(#[CurrentUser] User $user): JsonResponse
    {
        $shoppingItems = $this->shoppingItemService->getShoppingItemsByUser($user);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($shoppingItems)], Response::HTTP_OK);
    }

    #[Route('', name: 'create', methods: 'POST')]
    public function create(#[MapRequestPayload] ShoppingItemDto $shoppingItemDto, #[CurrentUser] User $user): JsonResponse
    {
        $shoppingItem = $this->shoppingItemService->save($shoppingItemDto, $user);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($shoppingItem)], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'update', methods: 'PUT')]
    public function update(#[MapRequestPayload] ShoppingItemDto $shoppingItemDto, ShoppingItem $shoppingItem): JsonResponse
    {
        $shoppingItemDto = $this->shoppingItemService->update($shoppingItemDto, $shoppingItem);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($shoppingItemDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    public function delete(ShoppingItem $shoppingItem): JsonResponse
    {
        $this->shoppingItemService->delete($shoppingItem);
        return $this->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
