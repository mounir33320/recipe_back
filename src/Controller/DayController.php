<?php

namespace App\Controller;

use App\Dto\DayDto;
use App\Entity\Day;
use App\Entity\User;
use App\Service\DayService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/days', name: 'days_')]
class DayController extends AbstractController
{
    public function __construct(private readonly DayService $dayService, private readonly NormalizerInterface $normalizer) {}

    #[Route('/init-days', name: 'init_week', methods: 'GET')]
    public function initDays(#[CurrentUser] User $user): JsonResponse
    {
        $days = $this->dayService->initDays($user);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($days)], Response::HTTP_OK);
    }

    #[Route('', name: 'index', methods: 'GET')]
    public function index(#[CurrentUser] User $user): JsonResponse
    {
        $days = $this->dayService->getDaysByUser($user);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($days)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'update')]
    public function update(#[MapRequestPayload] DayDto $dayDto, Day $day): JsonResponse
    {
        $day = $this->dayService->update($dayDto, $day);

        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($day)], Response::HTTP_OK);
    }
}
