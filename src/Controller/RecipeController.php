<?php

namespace App\Controller;

use App\Dto\RecipeDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Recipe;
use App\Entity\User;
use App\Repository\RecipeRepository;
use App\Repository\UserRepository;
use App\Service\RecipeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/recipes', name: 'recipes_')]
class RecipeController extends AbstractController
{
    public function __construct(private readonly RecipeService $recipeService, private readonly NormalizerInterface $normalizer) {}

    #[Route('', name: 'index', methods: 'GET')]
    public function index(#[CurrentUser] User $user): JsonResponse
    {
        $recipes = $this->recipeService->getRecipesByUser($user);
        return $this->json(['success' => true, 'data' => $recipes], Response::HTTP_OK);
    }

    #[Route('', name: 'create', methods: 'POST')]
    public function create(#[MapRequestPayload] RecipeDto $recipeDto, #[CurrentUser] User $user): JsonResponse
    {
        $recipe = $this->recipeService->save($recipeDto, $user);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($recipe)], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'show', methods: 'GET')]
    public function show(Recipe $recipe, DtoConverter $dtoConverter): JsonResponse
    {
        $recipeDto = $dtoConverter->convertToDto($recipe);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($recipeDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'update', methods: 'PUT')]
    public function update(#[MapRequestPayload] RecipeDto $recipeDto, Recipe $recipe): JsonResponse
    {
        $recipeDto = $this->recipeService->update($recipeDto, $recipe);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($recipeDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    public function delete(Recipe $recipe): JsonResponse
    {
        $this->recipeService->delete($recipe);
        return $this->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
