<?php

namespace App\Controller;

use App\Dto\IngredientDto;
use App\DtoConverter\DtoConverter;
use App\Entity\Ingredient;
use App\Entity\User;
use App\Service\IngredientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/api/ingredients', name: 'ingredients_')]
class IngredientController extends AbstractController
{
    public function __construct(private readonly IngredientService $ingredientService, private readonly NormalizerInterface $normalizer)
    {
    }

    #[Route('', name: 'index', methods: 'GET')]
    public function index(#[CurrentUser] User $user): JsonResponse
    {
        $ingredients = $this->ingredientService->getIngredientsByUser($user);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($ingredients), Response::HTTP_OK]);
    }

    #[Route('', name: 'create', methods: 'POST')]
    public function create(#[MapRequestPayload] IngredientDto $ingredientDto, #[CurrentUser] User $user): JsonResponse
    {
        $ingredientDto = $this->ingredientService->save($ingredientDto, $user);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($ingredientDto)], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'show', methods: 'GET')]
    public function show(Ingredient $ingredient, DtoConverter $dtoConverter): JsonResponse
    {
        $ingredientDto = $dtoConverter->convertToDto($ingredient);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($ingredientDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'update', methods: 'PUT')]
    public function update(#[MapRequestPayload] IngredientDto $ingredientDto, Ingredient $ingredient): JsonResponse
    {
        $ingredientDto = $this->ingredientService->update($ingredientDto, $ingredient);
        return $this->json(['success' => true, 'data' => $this->normalizer->normalize($ingredientDto)], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    public function delete(Ingredient $ingredient): JsonResponse
    {
        $this->ingredientService->delete($ingredient);
        return $this->json(['success' => true], Response::HTTP_NO_CONTENT);
    }
}
