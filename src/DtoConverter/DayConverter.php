<?php

namespace App\DtoConverter;

use App\Dto\DayDto;
use App\DtoConverter\DtoConverterInterface;
use App\Entity\Day;

class DayConverter implements DtoConverterInterface
{

    public function __construct(private readonly DtoConverter $dtoConverter)
    {
    }

    /**
     * @param Day $entity
     * @return DayDto
     */
    public function convertToDto(object $entity): object
    {
        $recipes = [];

        foreach ($entity->getRecipesDays() as $recipeDay) {

            $recipes[] = [
                'id' => $recipeDay->getRecipe()->getId(),
                'title' => $recipeDay->getRecipe()->getTitle(),
                'description' => $recipeDay->getRecipe()->getComment(),
                'ingredients' => $this->dtoConverter->convertArrayOfEntitiesToDtos($recipeDay->getRecipe()->getIngredients()),
                'categories' => $this->dtoConverter->convertArrayOfEntitiesToDtos($recipeDay->getRecipe()->getCategories()),
                'type' => $recipeDay->getType()
            ];
        }

        return new DayDto($entity->getId(), $entity->getName()->value, $entity->getDayNumber(), $recipes);
    }

    /**
     * @inheritDoc
     */
    public function convertToEntity(object $dto): object
    {
        // TODO: Implement convertToEntity() method.
    }

    /**
     * @inheritDoc
     */
    public function supports(object $object): bool
    {
        return $object instanceof Day || $object instanceof DayDto;
    }
}
