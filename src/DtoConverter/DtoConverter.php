<?php

namespace App\DtoConverter;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class DtoConverter
{
    /**
     * @param DtoConverterInterface[] $converters
     */
    public function __construct(
        #[TaggedIterator('app.dto_converter')]
        private readonly iterable $converters
    ) {}

    public function convertToDto(object $entity): object
    {
        foreach ($this->converters as $converter) {
            if ($converter->supports($entity)) {
                return $converter->convertToDto($entity);
            }
        }

        throw new \Exception('No DTO Converter found');
    }

    public function convertToEntity(object $dto): object
    {
        foreach ($this->converters as $converter) {
            if ($converter->supports($dto)) {
                return $converter->convertToEntity($dto);
            }
        }

        throw new \Exception('No DTO Converter found');
    }

    public function convertArrayOfEntitiesToDtos(array|Collection $entities): array
    {
        $dtos = [];

        foreach ($entities as $entity) {
            $dtos[] = $this->convertToDto($entity);
        }

        return $dtos;
    }

    public function convertArrayOfDtosToEntities(array|Collection $dtos): array
    {
        $entities = [];

        foreach ($dtos as $dto) {
            $entities[] = $this->convertToEntity($dto);
        }

        return $entities;
    }
}
