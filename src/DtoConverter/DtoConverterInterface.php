<?php

namespace App\DtoConverter;

interface DtoConverterInterface
{
    /**
     * @param object $entity
     * @return object
     */
    public function convertToDto(object $entity): object;

    /**
     * @param object $dto
     * @return object
     */
    public function convertToEntity(object $dto): object;

    /**
     * @param object $object
     * @return bool
     */
    public function supports(object $object): bool;
}
