<?php

namespace App\DtoConverter;

use App\Dto\UserDto;
use App\Entity\User;

class UserConverter implements DtoConverterInterface
{
    /**
     * @param User $entity
     * @return UserDto
     * @throws \Exception
     */
    public function convertToDto(object $entity): UserDto
    {
        return new UserDto(
            $entity->getId(),
            $entity->getUsername(),
            null,
            $entity->getRoles(),
            $entity->isActive(),
            $entity->getLastConnection()?->format('d/m/Y H:i:s'));
    }

    /**
     * @param UserDto $dto
     * @return User
     */
    public function convertToEntity(object $dto): object
    {
        return (new User())
            ->setUsername($dto->username)
            ->setPassword($dto->password)
            ->setActive($dto->isActive)
            ->setRoles($dto->roles);
    }

    public function supports(object $object): bool
    {
        return $object instanceof User || $object instanceof UserDto;
    }
}
