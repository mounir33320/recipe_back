<?php

namespace App\DtoConverter;

use App\Dto\CategoryDto;
use App\Entity\Category;
use Doctrine\Common\Collections\Collection;

class CategoryConverter implements DtoConverterInterface
{
    /**
     * @param Category $entity
     * @return CategoryDto
     * @throws \Exception
     */
    public function convertToDto(object $entity): object
    {

        return new CategoryDto($entity->getId(), $entity->getName(), $entity->getUser()->getId());
    }

    public function convertCollectionToDtoArray(Collection|array $entities): array
    {
        $dtoArray = [];

        /**
         * @var Category $entity
         */
        foreach ($entities as $entity) {
            $dtoArray[] = $this->convertToDto($entity);
        }

        return $dtoArray;
    }

    /**
     * @param CategoryDto $dto
     * @return Category
     * @throws \Exception
     */
    public function convertToEntity(object $dto): Category
    {
        return (new Category())->setName($dto->name);
    }

    public function supports(object $object): bool
    {
        return $object instanceof CategoryDto || $object instanceof Category;
    }
}
