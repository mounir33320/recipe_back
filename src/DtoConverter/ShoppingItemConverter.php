<?php

namespace App\DtoConverter;

use App\Dto\ShoppingItemDto;
use App\Entity\ShoppingItem;

class ShoppingItemConverter implements DtoConverterInterface
{

    /**
     * @param ShoppingItem $entity
     * @return ShoppingItemDto
     */
    public function convertToDto(object $entity): object
    {
        return new ShoppingItemDto($entity->getId(), $entity->getName(), $entity->isActive());
    }

    /**
     * @param ShoppingItemDto $dto
     * @return ShoppingItem
     */
    public function convertToEntity(object $dto): object
    {
        return (new ShoppingItem())
            ->setName($dto->name)
            ->setActive($dto->isActive)
        ;
    }

    public function supports(object $object): bool
    {
        return $object instanceof ShoppingItem || $object instanceof ShoppingItemDto;
    }
}
