<?php

namespace App\DtoConverter;

use App\Dto\IngredientDto;
use App\Entity\Ingredient;
use Doctrine\Common\Collections\Collection;

class IngredientConverter implements DtoConverterInterface
{
    public function __construct(private readonly DtoConverter $dtoConverter)
    {
    }

    /**
     * @param IngredientDto $dto
     *
     * @return Ingredient
     *
     * @throws \Exception
     */
    public function convertToEntity(object $dto): object
    {
        return (new Ingredient())->setName($dto->name);
    }

    /**
     * @param Ingredient $entity
     *
     * @return IngredientDto
     *
     * @throws \Exception
     */
    public function convertToDto(object $entity): object
    {
        return new IngredientDto($entity->getId(), $entity->getName(), $entity->getUser()->getId());
    }

    public function convertCollectionToDtoArray(array|Collection $entities): array
    {
        $dtoArray = [];

        /**
         * @var Ingredient $entity
         */
        foreach ($entities as $entity) {
            $dtoArray[] = $this->convertToDto($entity);
        }

        return $dtoArray;
    }

    public function supports(object $object): bool
    {
        return $object instanceof IngredientDto || $object instanceof Ingredient;
    }
}
