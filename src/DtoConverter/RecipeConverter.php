<?php

namespace App\DtoConverter;

use App\Dto\RecipeDto;
use App\Entity\Recipe;

class RecipeConverter implements DtoConverterInterface
{
    public function __construct(
        private readonly IngredientConverter $ingredientConverter,
        private readonly CategoryConverter $categoryConverter)
    {
    }

    /**
     * @param RecipeDto $dto
     * @return Recipe
     * @throws \Exception
     */
    public function convertToEntity(object $dto): object
    {
        return (new Recipe())
            ->setTitle($dto->title)
            ->setComment($dto->comment)
            ;
    }

    /**
     * @param Recipe $entity
     * @return RecipeDto
     * @throws \Exception
     */
    public function convertToDto(object $entity): object
    {
        return new RecipeDto(
            $entity->getId(),
            $entity->getTitle(),
            $entity->getComment(),
            $entity->getUser()->getId(),
            $this->ingredientConverter->convertCollectionToDtoArray($entity->getIngredients()->toArray()),
            $this->categoryConverter->convertCollectionToDtoArray($entity->getCategories()->toArray()));
    }

    public function supports(object $object): bool
    {
        return $object instanceof RecipeDto || $object instanceof Recipe;
    }
}
