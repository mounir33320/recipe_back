<?php

namespace App\Listener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionListener
{
    public function __invoke(ExceptionEvent $event): void
    {
        if ($event->getThrowable() instanceof HttpException && 401 === $event->getThrowable()->getStatusCode()) {
            $response = new JsonResponse(['success' => false, 'error' => $event->getThrowable()->getMessage()]);
            $event->setResponse($response);
        }
    }
}
