<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231104220849 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE day (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(15) NOT NULL, day_number INT NOT NULL, INDEX IDX_E5A02990A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recipe_day (id INT AUTO_INCREMENT NOT NULL, day_id INT NOT NULL, recipe_id INT NOT NULL, type VARCHAR(15) NOT NULL, INDEX IDX_94F74DDC9C24126 (day_id), INDEX IDX_94F74DDC59D8A214 (recipe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE day ADD CONSTRAINT FK_E5A02990A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE recipe_day ADD CONSTRAINT FK_94F74DDC9C24126 FOREIGN KEY (day_id) REFERENCES day (id)');
        $this->addSql('ALTER TABLE recipe_day ADD CONSTRAINT FK_94F74DDC59D8A214 FOREIGN KEY (recipe_id) REFERENCES recipe (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE day DROP FOREIGN KEY FK_E5A02990A76ED395');
        $this->addSql('ALTER TABLE recipe_day DROP FOREIGN KEY FK_94F74DDC9C24126');
        $this->addSql('ALTER TABLE recipe_day DROP FOREIGN KEY FK_94F74DDC59D8A214');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE recipe_day');
    }
}
